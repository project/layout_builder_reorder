<?php

namespace Drupal\layout_builder_reorder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MoveLayoutBuilderSectionController extends ControllerBase {

  use LayoutRebuildTrait;

  /**
   * The layout tempstore repository.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstoreRepository;

  /**
   * Constructs a new AddSectionToLibraryForm.
   */
  public function __construct(LayoutTempstoreRepositoryInterface $layout_tempstore_repository) {
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_builder.tempstore_repository')
    );
  }

  public function __invoke(SectionStorageInterface $section_storage, $delta, $new_delta) {
    // Rearrange sections
    $sections = $section_storage->getSections();
    if (array_key_exists($new_delta, $sections)) {
      $temp = $sections[$delta];
      $sections[$delta] = $sections[$new_delta];
      $sections[$new_delta] = $temp;
    }

    // Save new order
    $section_storage->removeAllSections();
    foreach ($sections as $section) {
      $section_storage->appendSection($section);
    }
    $this->layoutTempstoreRepository->set($section_storage);

    // Rebuild UI
    return $this->rebuildLayout($section_storage);
  }

}
