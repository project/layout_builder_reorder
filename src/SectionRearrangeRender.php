<?php

namespace Drupal\layout_builder_reorder;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;

class SectionRearrangeRender implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRender'];
  }

  /**
   * Pre-render callback for layout builder.
   */
  public static function preRender($elements) {
    if (isset($elements['layout_builder'])) {
      $sections = $elements['layout_builder'];
      foreach ($sections as $key => $section) {
        // Filter the sections.
        if (!is_numeric($key)) {
          continue;
        }

        // Add rearrange buttons
        if (isset($section['configure']) && isset($section['configure']['#url'])) {
          $params = $section['configure']['#url']->getRouteParameters();

          $route_parameters = [
            'section_storage_type' => $params['section_storage_type'],
            'section_storage' => $params['section_storage'],
            'delta' => $params['delta'],
          ];

          $options = [
            'attributes' => [
              'class' => [
                'use-ajax',
                'layout-builder__link',
                'layout-builder__link--rearrange',
              ]
            ],
          ];

          // Pop the last item to add the link before it.
          $last_item_section = array_pop($sections[$key]);
          $sections[$key]['rearrange'] = [
            '#type' => 'container'
          ];

          // Up link
          $move_up_url = Url::fromRoute(
            'layout_builder_reorder.move_section',
            array_merge($route_parameters, ['new_delta' => $params['delta'] - 1]),
            $options
          );
          $sections[$key]['rearrange']['up'] = [
            '#type' => 'link',
            '#title' => t('Move up'),
            '#url' => $move_up_url,
            '#access' => $move_up_url->access(),
          ];

          // Down link
          $move_down_url = Url::fromRoute(
            'layout_builder_reorder.move_section',
            array_merge($route_parameters, ['new_delta' => $params['delta'] + 1]),
            $options
          );
          $sections[$key]['rearrange']['down'] = [
            '#type' => 'link',
            '#title' => t('Move down'),
            '#url' => $move_down_url,
            '#access' => $move_down_url->access(),
          ];

          // Push the last item again.
          array_push($sections[$key], $last_item_section);
        }
      }

      $elements['layout_builder'] = $sections;
    }
    return $elements;
  }

}
